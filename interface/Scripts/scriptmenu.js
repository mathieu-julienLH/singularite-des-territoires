$(document).ready(function () {
    // On cache les sous-menus
    $(".navigation ul.subMenu").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".navigation li.toggleSubMenu span").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });

    $(".navigation ul.subsubMenu").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".navigation li.toggleSubSubMenu span").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });

    // On modifie l'évènement "click" sur les liens dans les items de liste
    // qui portent la classe "toggleSubMenu" :
    $(".navigation li.toggleSubMenu > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.subMenu:visible").length != 0) {
            $(this).next("ul.subMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
            $(".navigation ul.subMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
            $(this).next("ul.subMenu").slideDown("normal", function () {
                $(this).parent().addClass("open")
            });
        }
        // On empêche le navigateur de suivre le lien :
        return false;
    });

    $(".navigation li.toggleSubSubMenu > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.subsubMenu:visible").length != 0) {
            $(this).next("ul.subsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
            $(".navigation ul.subsubMenu").slideUp("normal", function () {
                $(this).parent().removeClass("open")
            });
            $(this).next("ul.subsubMenu").slideDown("normal", function () {
                $(this).parent().addClass("open")
            });
        }
        // On empêche le navigateur de suivre le lien :
        return false;
    });

    $(".close").css("display", "none");

    var isMenuOpen = false;
    var isNavOpen = true;
    var isNavFull = false;

    $('.menu_btn').click(function () {
        if (isMenuOpen == false) {
            //alert('je suis dans le bon cas')
            $("#menu").clearQueue().animate({
                left: '0'
            })
            $("#page").clearQueue().animate({
                "margin-right": '-400px'
            })

            //                    $(this).fadeOut(200);
            $(".close").fadeIn(300);

            isMenuOpen = true;
        } else {
            $("#menu").clearQueue().animate({
                left: '-400px'
            })
            $("#page").clearQueue().animate({
                "margin-right": '0px'
            })

            $(this).fadeOut(200);
            $(".menu_btn").fadeIn(300);

            isMenuOpen = false;
        }
    });

});
