$(document).ready(function () {
    var deroulant = [
        {
            'titre': 'OUTILS',
            'class': 'menu',
            'children': [
                {
                    'titre': 'Arborescence',
                    'lien': 'ArborescenceLien',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Riegl',
                    'lien': 'RieglLien',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Tabulations',
                    'lien': 'TabulationsLien',
                    'class': 'sousmenu',

                },
                {
                    'titre': 'Bibliographie',
                    'lien': 'bibliographie.html',
                    'class': 'sousmenu',

		},
                {
                    'titre': 'Cartographies',
                    'lien': 'TabulationsLien',
                    'class': 'sousmenu',

		}
                ]
	}, {
            'titre': 'TERRITOIRE',
            'class': 'menu',
            'children': [{
                    'titre': 'Urbino',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2009',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Urbino'
    }, {
                        'titre': '2010',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Urbino'
    }, {
                        'titre': '2011',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Urbino'
    }],

    }, {
                    'titre': 'Lisbonne',
                    'lien': 'RieglLien',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2012',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Lisbonne'
    }, {
                        'titre': '2013',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Lisbonne'
    }, {
                        'titre': '2014',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Lisbonne'
    }, {
                        'titre': '2015',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Lisbonne'
    }],
                },
                {
                    'titre': 'Athènes',
                    'lien': 'TabulationsLien',
                    'class': 'sous-menu-déroulant',
                    'children': [{
                        'titre': '2016',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Athènes'
    }, {
                        'titre': '2017',
                        'class': 'sous-sous-menu',
                        'lien': 'bibliographie.html',
                        'parent': 'Athènes'
    }, {
                        'titre': '2018',
                        'class': 'sous-sous-menu',
                        'lien': 'index2018.html',
                        'parent': 'Athènes'
    }]

                            }]
                    }];






    deroulant.forEach(function (e) {

        var nav = $('<li class="toggleSubMenu"><span>' + e['titre'] + '</span></li>').appendTo($('#navig'));
        console.log(nav);

        var nav_ul = $('<ul class="subMenu"> </ul>').appendTo(nav);
        for (var i in e.children) {
            var c = e.children[i];
            if (!c.children) {
                nav_ul.append('<li><a href="' + c['lien'] + '">' + c['titre'] + '</a></li>');
            } else {
                var nav2 = $('<li class="toggleSubSubMenu"><span>' + c['titre'] + '</span></li>').appendTo(nav_ul);
                var nav_ul2 = $('<ul class="subsubMenu"> </ul>').appendTo(nav2);
                for (var j in c.children) {
                    nav_ul2.append('<li><a href="' + c.children[j]['lien'] + '">' + c.children[j]['titre'] + '</a></li>');
                }

            }

        }






    });


    console.log(deroulant);

    var header = $('<strong><a href="Index.html">Singularités des territoires </a></strong><font><a href ="http://morpholab.fr/fr/home/"> | MorphoLab</a></font >').appendTo($('#head'))


});
